FROM jekyll/jekyll:4.2.0

COPY Gemfile /srv/jekyll
COPY Gemfile.lock /src/jekyll

RUN bundle install