.PHONY: push

push:
	git add .
	git commit -m "$(MESSAGE)"
	git push 

	docker exec -ti jojo-garden jekyll build -d _build/
	rm -rf /tmp/_build
	mv _build /tmp

	git checkout pages
	cp -aRv /tmp/_build/* .

	git add .
	git commit -m "$(MESSAGE)"
	git push 

	git checkout main