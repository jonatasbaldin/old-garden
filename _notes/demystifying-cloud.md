# Demystifying the Cloud

## Who am I talking to?
People using the cloud to develop and people developing the cloud itself.
    - Developers, sysadmins, devops, SRE

## Ideas
More tech:
    - Critic of cloud computing and ICT environment
    - The invisibility of the cloud, it is actually made of very real and physical components
    - How the invisibility of the Cloud is contributing to the environment destruction
    - Datacenter energy usage, how much it is fossil fuels
        - Are datacenters really carbon neutral?
    - Elastic computing and storage fuels eternal economic growth 
    - IT people must understand their responsibility when working on global scale systems
Social:
    - People should be aware of their responsibility as Internet users
    - Eternal economic growth leads to massive natural resource consumption, and eventually mass extinction
    - Capital accumulation leads to increasingly inequality and social struggles