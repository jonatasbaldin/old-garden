---
title: Cleaning up digital accounts
---

We all have accounts on dozens (hundreds?!) of platforms. In its peak, my password manager had almost 200 entries, 80% useless. All this idle data is still sitting in databases and servers somewhere, using natural resources to be stored, analyzed, migrated etc.

Here I want to document the process of deleting my accounts and their data from services I don't use anymore.

### [Americanas](https://americanas.com.br)
- Sent email as specified in the account details
- Received a weird email response, not actually addressing my request
- Sent another email
- Still waiting after weeks

### [Bitwage](https://bitwage.com)G
- Lost access to account with 2FA enabled
- Emailed their support to get backup codes
- Was asked to confirm personal information, which I did
- I got 2FA backup codes
- Could not delete my account and data through the platform 🤦
- Sent a message to their support
- Got my account deleted

### [Cloudflare](https://cloudflare.com)
- Accessed the platform
- Somewhere on Settings > Delete my Data
- Done

### [Credly](https://creddly.com)
- Accessed the platform
- Somewhere on Settings > Delete my Data
- Done

### [Duolingo](duolingo.com/)
- Accessed the platform
- Somewhere on Settings > Delete my Data
- Done

### [Facebook](https://facebook.com)
Long time ago. I made a backup and went somewhere on Settings > Delete My Account.

### [Hacker News](https://news.ycombinator.com)
- Sent email to hn@ycombinator.com
- Got an answer stating `deleted the posts, scrubbed the profile, and randomized the username`, good enough

### [Instagram](https://instagram.com)
Long time ago. I made a backup and went somewhere on Settings > Delete My Account. I remember I could not do through the app, only on the web app, which I consider a dark pattern.

### [Manning](https://manning.com)
- Sent email to their support
- Got my account removed

### [PSI](https://www.psionline.com/en-gb/)
- Requested the data removal through their support
- They asked me to request the data deletion to the organization who asked me for the assessments
- Asked my employer

### [RescueTime](https://www.rescuetime.com)
- Closed through their platform, Settings > Delete Account

### [Restream](https://restream.io)
- Closed my account through their platform
- Confirmed by clicking on an email button
- Done

### [Runkeeper](https://runkeeper.com/delete-account)
- Go to https://runkeeper.com/delete-account
- Choose your country
- Delete button
- Done

### [Server Fault](https://serverfault.com)
- Requested account deletion through their platform
- Received confirmation by email

### [Tiptrans](https://tiptrans.com)
- Sent a message through their contact form asking to delete my account and data
- They did
- Done

### [Twilio](https://twilio.com)
- Tried to delete my account through the platform, was not able due negative balance of $5
- Added $20 (their minimum, currently), paid my bills
- Closed my account, got confirmation emails
- Got a refund from the positive balance

### [Twitch](https://twitch.tv)
- Go to https://www.twitch.tv/user/delete-account
- Click the Delete button
- Done

### [Unroll.me](https://unroll.me)
- Didn't have access to the account
- Found an [article explaining how to delete an account](https://support.unroll.me/hc/en-us/articles/360060115832-How-do-I-delete-my-Unroll-Me-Account-) through their FAQ
- Entered my email on [this page](https://unroll.me/a/delete-my-account)
- Verified the code sent to my email
- Done

### [Urban Sports Club](https://urbansportsclub.com)
- Opened a ticket through their support page
- Never received an answer but got my data deleted 🤷‍♀️

### [Vuforia](https://developer.vuforia.com/)
- Sent a message through their contact form
- I was asked to delete all my licenses on the platform to confirm my request, which I did
- Account was deleted

### Never got an answer
- WorldNomads
- Zambia eServices
