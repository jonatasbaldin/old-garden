---
title: Moving away from big tech
categories: tech activism bigtech
toc: 'yes'
---

Stop using services from big tech corporations (Google, Microsoft, Amazon, Facebook etc).

I want to stop using big tech corps.  
I want to stop using closed source.  
I want to show people alternatives to the big corp slavery.  
I want to find alternatives (even if proprietaries), as long as they are provided by ethical companies.  

Characteristics I value:
- Open source
- Privacy protection
- Possibility to self-host
- Ad-free (or responsible ads)

Resources:
- https://github.com/pluja/awesome-privacy
- https://privacyguides.org/

# 2FA Authenticator
Moved away from Google Authenticator.

Using: 
- [RavioOTP](https://github.com/raivo-otp/) is _way better_ than Google Authenticator. It has better export/import functions, design and is open source. https://privacyguides.org

# Cloud Storage
Moving away from Google Drive. 

This one is tricky, but it is happening. Here is what I'm doing.

## Documents
Here are things like Markdown notes, .doc and PDF. I don't have much, like 200Mb. I do have other old archives that were on Google Drive (couple of GB), which I backed up on an external hard drive and [Premiumize.me](https://www.premiumize.me/ref/114686856).
- I store the notes locally on a folder on my laptop
- I sync this folder with [Syncthing](https://syncthing.net/) to my Raspberry Pi
- On my iPhone, I access the Raspberry Pi folder via a [Samba](https://www.samba.org/) share and use the [Koder](https://koderapp.com/) app to access / edit the files
	- I have a [Wireguard](https://www.wireguard.com/) VPN with [PiVPN](https://docs.pivpn.io/wireguard/) to my Raspberrpy Pi, so I can access the notes remotely outside from my network
- Backups are made daily to [Premiumize.me](https://www.premiumize.me/ref/114686856), everything is encrypted using [age](https://github.com/FiloSottile/age)
- **I still need an office suite solution. Might go with [ONLYOFFICE](https://onlyoffice.com/) or [Cryptpad](cryptpad.fr/), both self-hosted**

## Photos
Still using Google Photos.

# Email and Calendar
Moving away from Gmail and using [Protonmail](https://protonmail.com/) with my own domain. I am migrating manually, so it will take some time to change all my accounts to my new email.

# Maps
Still using Google Maps.

I tried [OsmAnd](https://osmand.net/) for a while but switched back to Google Maps for now. I believe this will be the hardest transition, since Google Maps is so good at public transport and quickly finding places and information about them.

Also, [avoid MAPS.ME](https://wiki.openstreetmap.org/wiki/MAPS.ME#Questionable_edits).

# Password manager
Moved away from 1Password to use [KeePassXC](https://keepassxc.org/).

What I did:
- [Exported data from 1Password](https://support.1password.com/export/) in CSV
- Imported data into KeePassXC (match the CSV columns when loading the CSV)
- [Enable browser integration](https://keepassxc.org/docs/KeePassXC_GettingStarted.html#_setup_browser_integration)

I store the KeePassXC file on my Raspberry Pi and share it with a Samba share. I access the Samba share from my laptop and use the official KeePassXC client. Also, I access the share on my iPhone and access the file with [KeePassium](https://keepassium.com/).

# Streaming services
Moving away from Netflix, Disney+ and Spotify.

Using:
- [Jellyfin](https://jellyfin.org) as a media center, hosted on a local Raspberry Pi, storing files on an external hard drive mounted on the Raspberry Pi
- [Premiumize.me](https://www.premiumize.me/ref/114686856) to download movies, albums etc
- [VLC](https://www.videolan.org) on my iPhone to cast Jellyfin to a Chromecast
    - Getting rid of the iPhone and Chromecast soon 🤞
    - [Infuse](https://firecore.com/infuse) is also quite good
- [Finamp](https://github.com/UnicornsOnLSD/finamp) as an audio client for Jellyfin
