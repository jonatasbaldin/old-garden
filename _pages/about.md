---
layout: page
title: about
permalink: /about
---

Hi there, I'm Jonatas Baldin, also known as jojo.

I'm a human exploring how to live more with less, tired of capitalism exploration and constantly trying to rediscover the self. I live in Berlin with my little son and my partner.

Oh, I also exchange my hours for money as a software and infrastructure engineer, especially if you are not into eternal economic growth, profit driven business and labor exploitation.

Get in touch via <a href="{{ site.email }}">email</a> or <a href="{{ site.mastodon }}">mastodon</a>.