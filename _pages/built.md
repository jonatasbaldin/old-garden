---
layout: page
title: built with
permalink: /built-with
---

- [Digital Garden Jekyll Template](https://github.com/maximevaillancourt/digital-garden-jekyll-template)
- [Codeberg Pages](https://codeberg.page/)
- [List.js](https://listjs.com/)