---
layout: page
title: deployeveryday
id: deployeveryday
permalink: /deployeveryday
link_preview: true
---

These are the posts from my old blog, previously hosted at [deployeveryday.com](https://deployeveryday.com).

{% for item in site['deployeveryday'] %}
<div class="note-index"><a class="internal-link" href="{{ item.url }}">{{ item.path | replace: "_deployeveryday", ""}}</a></div>
{% endfor %}