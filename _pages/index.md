---
title: ''
layout: page
id: home
permalink: /
---

Welcome to my personal garden, feel free to explore.

<div id="db" class="db">
  <input class="search" placeholder="Search here..."/>

  {% assign entries = site.data.db.entries %}

  <div class="tag-list">
  {% assign tags = "" | split: "," %}

  {% for entry in entries %}
    {% assign entry_tags = entry.tags | split: " " %}
    {% for t in entry_tags %}
      {% assign tags = tags | uniq | push: t %}
    {% endfor %}
  {% endfor %}

  {% for tag in tags %}
    <span class="tag">{{ tag }}</span>
  {% endfor %}
  </div>

  <ul class="list">
      {% for entry in entries %}
          {% if entry.url %}<a href="{{ entry.url }}" target="_blank">{% endif %}
            <li id="{{ entry.name | slugify }}">
              <h2 class="name">{{ entry.name }}</h2>

              {% if entry.address != "" %}
                <p class="address">{{ entry.address }}</p>
              {% endif %}

              {% if entry.description != "" %}
                <p class="description">{{ entry.description }}</p>
              {% endif %}

              {% assign visited = nil %}
              {% if entry.visited == true %}
                {% assign visited =  "Visited: ✅" %}
              {% elsif entry.visited == false %}
                {% assign visited =  "Visited: 🚫" %}
              {% endif %}

              {% assign go_again = nil %}
              {% if entry.go_again == true %}
                {% assign go_again = "Go again: ✅" %}
              {% elsif entry.go_again == false %}
                {% assign go_again = "Go again: 🚫" %}
              {% endif %}

              {% if visited or go_again %}
                <p>{{ visited }} {{ go_again }}</p>
              {% endif %}

              {% if entry.type == "book" %}
                <p>Read: {% if entry.book_read == "yes" %}✅{% elsif entry.book_read == "no" %}🚫{% elsif entry.book_read == "reading" %}🤓{% endif %}</p>
              {% endif %}

              {% if entry.type == "image" %}
                <img src="{{ entry.img_url }}" />
              {% endif %}

              {% assign tags = entry.tags | split: " " %}

              {% for tag in tags %}
                <span class="tag tags" data="{{ entry.tags }}">{{ tag }}</span>
              {% endfor %}
            </li>
          {% if entry.url %}</a>{% endif %}
      {% endfor %}
    </ul>
</div>

<script>

var options = {
    valueNames: ['name', 'url', 'description', 'address', { attr: 'data', name: 'tags'}]
};

var dbList = new List('db', options);

</script>
