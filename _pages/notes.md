---
layout: page
title: notes
id: notes
permalink: /notes
link_preview: true
---

My notes, drafts and long thoughts.

{% assign deployeveryday = site.collections | where: "label", "deployeveryday" | first %}
<div class="note-index"><a class="internal-link" href="{{ site.baseurl }}/deployeveryday">{{ deployeveryday.relative_directory }}/</a></div>

{% for item in site['notes'] %}
<div class="note-index"><a class="internal-link" href="{{ item.url }}">{{ item.path }}</a></div>
{% endfor %}
