---
title: webring
layout: page
id: webring
permalink: /webring
---

{% assign entries = site.data.links | jsonify %}

<script>
    links = {{ entries }}

    link = links[Math.floor(Math.random() * links.length)]
    window.location.href = link.link
</script>