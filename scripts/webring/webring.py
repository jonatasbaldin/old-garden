import requests
import yaml
from bs4 import BeautifulSoup


links = []

hotline_webring = requests.get('https://hotlinewebring.club/')
hotline_soup = BeautifulSoup(hotline_webring.content, 'html.parser')

for url in hotline_soup.find_all(class_='url'):
    links.append({
        'link': url.a.get('href'),
        'webring': 'hotline',
    })

xxiivv_webring = requests.get('https://webring.xxiivv.com/')
xxiivv_soup = BeautifulSoup(xxiivv_webring.content, 'html.parser')

for url in xxiivv_soup.ol.find_all('a'):
    links.append({
        'link': url.get('href'),
        'webring': 'xxiivv',
    })


with open('../../_data/links.yml', 'w') as file:
    yaml.dump(links, file)
